from functools import reduce
from operator import add

from notetion.midi import event_bytes
from notetion.midi.midi_header import header_bytes, track_bytes
from notetion.parser import Parser
from notetion.scanner import Scanner
from notetion.semantics.semantics_parser import semantics

if __name__ == '__main__':
    scanner = Scanner("shortmelody.txt")
    parser = Parser(scanner)
    events = list(semantics(parser))

    header = header_bytes()
    midi = list(map(event_bytes, events))
    track_header = track_bytes(len(reduce(add, midi)))
    print("HEADER")
    print(header.hex())
    print("TRACK HEADER")
    print(track_header.hex())
    print("EVENTS")
    for m, e in zip(midi, events):
        print(e)
        print(m.hex())
