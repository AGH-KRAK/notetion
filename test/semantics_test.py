from notetion.parser import Parser
from notetion.scanner import Scanner
from notetion.semantics.semantics_parser import semantics

if __name__ == '__main__':
    scanner = Scanner("shortmelody.txt")
    parser = Parser(scanner)
    midi_events = semantics(parser)
    for x in midi_events:
        print(x)
