
class Scanner(object):
    def __init__(self, path):
        self.tokens = []
        self.line = ""
        self.source_file = open(path, "r")
        self.line_number = 0

    def __iter__(self):
        return self

    def __next__(self):
        return self.next()

    def next(self):
        self.line_number += 1
        self.line = self.source_file.readline()
        if self.line == '':
            raise StopIteration()
        while self.line[0] == "\n" or self.line[0] == "\r":
            self.line = self.source_file.readline()
            if self.line == '':
                raise StopIteration()
        token_start = 0
        self.tokens = []
        while token_start < len(self.line):
            a = self.line[token_start]
            if a == '[':
                self.tokens.append('[')
                while True:
                    token_start += 1
                    token_length = 0
                    while self.line[token_start].isspace():
                        token_start += 1
                    while token_start+token_length < len(self.line) and (self.line[token_start+token_length].isalnum() or self.line[token_start+token_length] == '-'):
                        token_length += 1
                    if self.line[token_start+token_length].isspace():
                        self.tokens.append(self.line[token_start:token_start+token_length])
                        token_start += token_length
                        continue
                    elif self.line[token_start+token_length] == ']':
                        if token_length > 0:
                            self.tokens.append(self.line[token_start:token_start+token_length])
                        self.tokens.append(']')
                        token_start += token_length + 1
                        break
                    self.error(token_start+token_length)
            elif a.isspace() or a == '|':
                token_start += 1

            elif a == '{':
                self.tokens.append('{')
                token_start += 1
                while self.line[token_start] != '}':
                    token_start = self.scan_note(token_start)
                self.tokens.append('}')
                token_start += 1

            elif a.isalnum() or a == ',' or a == '_':
                token_start = self.scan_note(token_start)
            else:
                self.error(token_start)

        return self.tokens

    def scan_note(self, token_start):
        c = self.line[token_start]

        if 'a' <= c <= 'g' or 'A' <= c <= 'G':
            self.tokens.append(c)
            token_start += 1
            if self.line[token_start] == 'b' or self.line[token_start] == '#':
                self.tokens.append(self.line[token_start])
                token_start += 1
            while self.line[token_start] == '^' or self.line[token_start] == 'v':
                self.tokens.append(self.line[token_start])
                token_start += 1
            if self.line[token_start] == "+":
                self.tokens.append(self.line[token_start])
                token_start += 1
                return self.scan_note(token_start)
        elif self.line[token_start] == ',' or self.line[token_start] == '_':
            self.tokens.append(self.line[token_start])
            token_start += 1
        else:
            self.error(token_start)

        if self.line[token_start] == '\\':
            self.tokens.append('\\')
            token_start += 1
        elif self.line[token_start] == '/':
            self.tokens.append('/')
            token_start += 1
            while self.line[token_start] == '\'':
                self.tokens.append('\'')
                token_start += 1
        else:
            self.error(token_start)

        while self.line[token_start] == '.':
            self.tokens.append('.')
            token_start += 1
        return token_start

    def error(self, token_start):
        raise ValueError("Scan error at line " + str(self.line_number)
                         + " character " + str(token_start))
