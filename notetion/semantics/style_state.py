from notetion.grammar import style
from notetion.grammar.types import Meta, Tempo, TimeSignature, KeySignature, MetaOctave, Force, NoteHold


class StyleState:
    """
    Preserves information about the default way to play each note

    Can be updated with Meta tags
    """
    def __init__(self):
        self.bpm = 120
        self.time_signature = (4, 4)
        self.sharps = set()
        self.flats = set()
        self.octave = 0
        self.volume = style.mf.volume
        self.duration = style.detache.fractional_duration

    def update(self, change: Meta):
        if isinstance(change, Tempo):
            self.bpm = change.bpm

        if isinstance(change, TimeSignature):
            self.time_signature = (change.upper, change.lower)

        if isinstance(change, KeySignature):
            self.sharps = change.sharps
            self.flats = change.flats

        if isinstance(change, MetaOctave):
            self.octave = change.octave

        if isinstance(change, Force):
            self.volume = change.volume

        if isinstance(change, NoteHold):
            self.duration = change.fractional_duration
