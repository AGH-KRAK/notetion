from functools import partial
from typing import Generator, Collection, Iterable

import attr

from notetion.midi import MIDIMetaEvent, meta_bytes
from notetion.midi.midi_note import MIDINote, NoteOn, NoteOff
from notetion.semantics.style_state import StyleState
from notetion.grammar.types import Word, Meta, Note, Pitch, Duration, Shift, Tempo, TimeSignature


def semantics(words: Iterable[Word]) -> Generator[MIDINote, None, None]:
    """Creates a time-sorted stream of note on/note off events"""
    style = StyleState()
    beat = 0  # always stored in quarter notes, for simplicity
    yield MIDIMetaEvent(beat, Tempo(style.bpm))
    yield MIDIMetaEvent(beat, TimeSignature(*style.time_signature))
    for word in words:
        if isinstance(word, Meta):
            style.update(word)
            meta_event = MIDIMetaEvent(beat, word)
            if len(meta_bytes(meta_event)) > 0:
                yield meta_event
                beat = 0

        if isinstance(word, Note):
            # noinspection PyTypeChecker
            pitches = map(
                partial(shift_octave, style.octave),
                word.pitches)

            pitches = list(map(
                partial(match_key, style.sharps, style.flats),
                pitches
            ))

            note_beats = to_beats(word.duration)
            note_length = note_beats * style.duration

            for pitch in pitches:
                yield NoteOn(time=beat, pitch=pitch, velocity=style.volume, channel=1)
                beat = 0

            first = True
            for pitch in pitches:
                yield NoteOff(time=note_length if first else 0,
                              pitch=pitch,
                              velocity=1-style.duration,
                              channel=1)
                first = False

            beat += note_beats - note_length


def shift_octave(octave: int, pitch: Pitch) -> Pitch:
    return attr.evolve(pitch, octave=pitch.octave + octave)


def match_key(sharps, flats, pitch: Pitch) -> Pitch:
    if pitch.natural in sharps:
        return attr.evolve(pitch, shift=Shift.Sharp)
    if pitch.natural in flats:
        return attr.evolve(pitch, shift=Shift.Flat)
    return pitch


def to_beats(durations: Collection[Duration]) -> float:
    """A sum of the durations, converted into a number of quarter notes"""
    def f(duration: Duration) -> float:
        base = 4 / duration.fraction
        dot_length = base
        beats = base
        for _ in range(duration.dots):
            dot_length /= 2
            beats += dot_length
        return beats

    return sum(map(f, durations))
