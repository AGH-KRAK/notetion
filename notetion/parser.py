from notetion.grammar import style, key_signature
from notetion.grammar.types import *


class Parser(object):
    def __init__(self, scanner):
        self.scanner = scanner
        self.line = ""
        self.current_token = 0

    def __iter__(self):
        return self

    def __next__(self):
        return self.next()

    def next(self):
        if self.current_token >= len(self.line):
            self.line = self.scanner.next()
            if self.line is None:
                raise StopIteration()
            self.current_token = 0
        if self.line[self.current_token] == '[':
            if self.line[self.current_token + 2] == ']':
                self.current_token += 3
                return self.parse_meta([self.line[self.current_token - 2]])
            else:
                last_token = 2
                while self.line[self.current_token + last_token] != ']':
                    last_token += 1
                tmp = self.parse_meta(self.line[self.current_token + 1:self.current_token + last_token])
                self.current_token += last_token + 1
                return tmp
        elif 'a' <= self.line[self.current_token] <= 'g' or 'A' <= self.line[self.current_token] <= 'G':
            pitches = [self.parse_pitch()]
            while self.line[self.current_token] == '+':
                self.current_token += 1
                pitches.append(self.parse_pitch())
            dur = self.parse_duration()
            return Note(pitches, dur)
        elif ',' == self.line[self.current_token]:
            self.current_token += 1
            dur = self.parse_duration()
            return Pause(duration=dur)
        else:
            raise SyntaxError("Token : " + str(self.line[self.current_token]))

    def parse_duration(self):
        if self.line[self.current_token] == '\\':
            dur = Duration(2)
            self.current_token += 1
        elif self.line[self.current_token] == '/':
            dur = Duration(4)
            self.current_token += 1
            while self.current_token < len(self.line) and self.line[self.current_token] == '\'':
                dur.fraction *= 2
                self.current_token += 1
        else:
            dur = Duration(1)
            self.current_token += 1
        while self.current_token < len(self.line) and self.line[self.current_token] == '.':
            dur.dots += 1
            self.current_token += 1
        if self.current_token < len(self.line) and self.line[self.current_token] == '_':
            self.current_token += 1
            return [dur] + self.parse_duration()
        return [dur]

    @staticmethod
    def parse_meta(metaargs):
        if len(metaargs) == 1:
            return style.by_name("".join(metaargs))
        if metaargs[0] == "tempo":
            return Tempo(int(metaargs[1]))
        elif metaargs[0] == "time":
            return TimeSignature(int(metaargs[1]), int(metaargs[2]))
        elif metaargs[0] == "octave":
            return MetaOctave(int(metaargs[1]))
        elif metaargs[0] == "key":
            if len(metaargs) == 2 and key_signature.by_name(metaargs[1]):
                return key_signature.by_name(metaargs[1])
            else:
                return KeySignature([Natural[n[0]] for n in metaargs if n[1] == '#'],
                                    [Natural[n[0]] for n in metaargs if n[1] == 'b'])
        elif metaargs[0] == "force":
            return Force(float(metaargs[1])/100)
        elif metaargs[0] == "notelength":
            return NoteHold(float(metaargs[1])/100)
        raise SyntaxError("Unknown meta argument: " + str(metaargs[0]))

    def parse_pitch(self):
        note_key = str(self.line[self.current_token])
        pitch = Pitch(Natural[note_key.upper()])
        self.current_token += 1
        if self.line[self.current_token] == '#':
            shift = Shift.Sharp
            self.current_token += 1
        elif self.line[self.current_token] == 'b':
            shift = Shift.Flat
            self.current_token += 1
        else:
            shift = Shift.Natural
        pitch.shift = shift
        if note_key.islower():
            pitch.octave = 5
            while self.line[self.current_token] == '^':
                pitch.octave += 1
                self.current_token += 1
        else:
            pitch.octave = 4
            while self.line[self.current_token] == 'v':
                pitch.octave -= 1
                self.current_token += 1
        return pitch
