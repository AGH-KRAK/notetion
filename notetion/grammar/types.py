from enum import Enum
from functools import partial
from typing import Set, List
import attr

Natural = Enum('Natural', 'C D E F G A B')


class Shift(Enum):
    Natural = 0
    Sharp = 1
    Flat = -1


@attr.s(auto_attribs=True)
class Pitch:
    natural: Natural
    shift: Shift = Shift.Natural
    octave: int = 4


@attr.s(auto_attribs=True)
class Duration:
    fraction: int  # 4 is quarter; 2 is half; 1 is whole; 16 is sixteenth; etc
    dots: int = 0


class Word:
    ...


@attr.s(auto_attribs=True)
class Note(Word):
    pitches: List[Pitch]  # multiple pitches can be played at once
    duration: List[Duration]  # can consist of multiple "attached" notes


Pause = partial(Note, pitches=[])


@attr.s(auto_attribs=True)
class Tuplet(Word):
    ...
    # todo; tuplets are weird


# region [meta] tags
class Meta(Word):
    ...


@attr.s(auto_attribs=True)
class Tempo(Meta):
    bpm: int


@attr.s(auto_attribs=True)
class TimeSignature(Meta):
    upper: int
    lower: int


@attr.s(auto_attribs=True)
class KeySignature(Meta):
    sharps: Set[Natural]
    flats: Set[Natural]


@attr.s(auto_attribs=True)
class MetaOctave(Meta):
    octave: int


@attr.s(auto_attribs=True)
class Force(Meta):
    volume: float


@attr.s(auto_attribs=True)
class NoteHold(Meta):
    fractional_duration: float
# endregion
