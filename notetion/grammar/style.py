from notetion.grammar.types import NoteHold, Force, Meta

staccato = NoteHold(0.15)
legato = NoteHold(0.95)
tenuto = NoteHold(0.8)
detache = NoteHold(0.55)

pianissimo = Force(0.2)
piano = Force(0.3)
mezzopiano = Force(0.4)
mezzoforte = Force(0.6)
forte = Force(0.8)
fortissimo = Force(0.95)

ppp = Force(0.1)
pp = pianissimo
p = piano
mp = mezzopiano
mf = mezzoforte
f = forte
ff = fortissimo
fff = Force(1.0)


def by_name(style_name: str) -> Meta:
    return globals()[style_name]
