from notetion.grammar.types import KeySignature, Natural

C = KeySignature(set(), set())
G = KeySignature({Natural.F}, set())
D = KeySignature({Natural.F, Natural.C, }, set())
A = KeySignature({Natural.F, Natural.C, Natural.G, }, set())
E = KeySignature({Natural.F, Natural.C, Natural.G, Natural.D, }, set())
B = KeySignature({Natural.F, Natural.C, Natural.G, Natural.D, Natural.A, }, set())
Fs = KeySignature({Natural.F, Natural.C, Natural.G, Natural.D, Natural.A, Natural.E, }, set())
Cs = KeySignature({Natural.F, Natural.C, Natural.G, Natural.D, Natural.A, Natural.E, Natural.B, }, set())

F = KeySignature(set(), {Natural.B, })
Bb = KeySignature(set(), {Natural.B, Natural.E, })
Eb = KeySignature(set(), {Natural.B, Natural.E, Natural.A, })
Ab = KeySignature(set(), {Natural.B, Natural.E, Natural.A, Natural.D, })
Db = KeySignature(set(), {Natural.B, Natural.E, Natural.A, Natural.D, Natural.G, })
Gb = KeySignature(set(), {Natural.B, Natural.E, Natural.A, Natural.D, Natural.G, Natural.C, })
Cb = KeySignature(set(), {Natural.B, Natural.E, Natural.A, Natural.D, Natural.G, Natural.C, Natural.F, })

a = C
e = G
b = D
fs = A
cs = E
gs = B
ds = Fs
eb = Gb
ab = Cb
asharp = Cs
bb = Db
f = Ab
c = Eb
g = Bb
d = F

C_MAJOR = C 
G_MAJOR = G 
D_MAJOR = D 
A_MAJOR = A 
E_MAJOR = E 
B_MAJOR = B 
FSHARP_MAJOR = Fs
CSHARP_MAJOR = Cs
F_MAJOR = F 
FLATFLAT_MAJOR = Bb
EFLAT_MAJOR = Eb
AFLAT_MAJOR = Ab
DFLAT_MAJOR = Db
GFLAT_MAJOR = Gb
CFLAT_MAJOR = Cb

A_MINOR = a
E_MINOR = e
B_MINOR = b
FSHARP_MINOR = fs
CSHARP_MINOR = cs
GSHARP_MINOR = gs
DSHARP_MINOR = ds
EFLAT_MINOR = eb
AFLAT_MINOR = ab
ASHARP_MINOR = asharp
BFLAT_MINOR = bb
F_MINOR = f
C_MINOR = c
G_MINOR = g
D_MINOR = d


def by_name(key_signature_name: str) -> KeySignature:
    key_signature_name = key_signature_name.replace("#", "s")
    if key_signature_name == "as":
        return asharp
    return globals()[key_signature_name]
