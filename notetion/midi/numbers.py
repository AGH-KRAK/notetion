def vlq(n: int) -> bytes:
    """Variable-Length Quantity

    Presents the number in 7-bit pieces; most significant first.
    The first bit (bit 7) of each byte is on, except for the last byte in the value.
    """
    assert n <= 0x0FFFFFFF  # MIDI files can only represent 32-bit vlq's

    # convert n to binary
    bits = '{0:b}'.format(n)

    # pad the front of the number with 0s
    padding = 7 - len(bits) % 7
    if padding != 7:
        bits = '0' * padding + bits

    # insert 1's every 7 bits
    bits = '1' + '1'.join((bits[i:i+7] for i in range(0, len(bits), 7)))
    # but replace the last inserted 1 with a 0.
    bits = bits[:-8] + '0' + bits[-7:]

    return bytes([int(bits[i:i+8], 2) for i in range(0, len(bits), 8)])


def byted_num(n: int, numbytes: int = 4) -> bytes:
    """A multi-byte unsigned number represented by its bytes

    most significant first (big-endian)
    """
    assert 0 <= n
    assert n >> (8*numbytes) == 0

    return bytes([(n >> (8*i)) % 256 for i in range(numbytes)][::-1])
