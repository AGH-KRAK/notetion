from enum import Enum
from functools import partial

import attr

from notetion.midi import MIDIEvent
from notetion.grammar.types import Pitch, Natural, Shift


MIDIEventType = Enum('MIDIEventType', 'NoteOn NoteOff')


@attr.s(auto_attribs=True)
class MIDINote(MIDIEvent):
    """
    A semantic defition for notes that matches how the MIDI file format views pitches and durations

    velocity is given as a portion of max (between 0 and 1)
    """

    pitch: Pitch
    velocity: float  # min 0, max 1
    channel: int
    event: MIDIEventType


NoteOn = partial(MIDINote, event=MIDIEventType.NoteOn)
NoteOff = partial(MIDINote, event=MIDIEventType.NoteOff)


def note_bytes(note: MIDINote) -> bytes:
    assert 0b0000 <= note.channel <= 0b1111

    status = _event_code[note.event] + note.channel

    return bytes([
            status,
            _midi_pitch(note.pitch),
            _midi_velocity(note.velocity),
        ])


# region helpers

_natural_offset = {
    Natural.C: 0,
    Natural.D: 2,
    Natural.E: 4,
    Natural.F: 5,
    Natural.G: 7,
    Natural.A: 9,
    Natural.B: 11,
}


_event_code = {
    MIDIEventType.NoteOn: 0b10010000,
    MIDIEventType.NoteOff: 0b10000000,
}


def _midi_pitch(pitch: Pitch):
    octave = pitch.octave + 1  # octave '-1' in midi is represented by 0
    mp = octave * 12 + _natural_offset[pitch.natural]

    if pitch.shift == Shift.Sharp:
        mp += 1
    if pitch.shift == Shift.Flat:
        mp -= 1

    assert 0 <= mp < 128
    return mp


def _midi_velocity(portion_of_max: float) -> int:
    return max(0, min(127, round(portion_of_max * 128)))

# endregion
