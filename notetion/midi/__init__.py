from notetion.midi.midi_event import MIDIEvent, MIDIMetaEvent, meta_bytes
from notetion.midi.midi_note import MIDINote, note_bytes
from notetion.midi.numbers import vlq


def event_bytes(event: MIDIEvent, midiclocks_per_quarter: float = 96) -> bytes:
    midi_clocks = round(midiclocks_per_quarter * event.time)
    ret = bytes()
    if isinstance(event, MIDINote):
        ret = note_bytes(event)
    if isinstance(event, MIDIMetaEvent):
        ret = meta_bytes(event)
    return vlq(midi_clocks) + ret if len(ret) > 0 else ret
