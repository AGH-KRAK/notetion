from notetion.midi.numbers import byted_num


def header_bytes() -> bytes:
    # todo: add arguments (format, ntracks, ticks per quarter)
    return bytes([
        # 4D 54 68 64 - "MThd"
        0x4D, 0x54, 0x68, 0x64,
        # 00 00 00 06 - header size
        0x00, 0x00, 0x00, 0x06,
        # 00 00 - format 0 (single track file)
        0x00, 0x00,
        # 00 01 - 1 track
        0x00, 0x01,
        # 00 60 - "96 ticks per quarter note"
        0x00, 0x60,
    ])


def track_bytes(chunk_length: int) -> bytes:
    return bytes([
        # 4D 54 72 6B - "MTrk"
        0x4D, 0x54, 0x72, 0x6B,
        # 4 bytes of chunk length
    ]) + byted_num(chunk_length, 4)
