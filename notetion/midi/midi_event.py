from math import log2

import attr

from notetion.midi.numbers import byted_num
from notetion.grammar.types import Meta, Tempo, TimeSignature, KeySignature


@attr.s(auto_attribs=True)
class MIDIEvent:
    time: float  # given in quarter notes since the last event


@attr.s(auto_attribs=True)
class MIDIMetaEvent(MIDIEvent):
    meta: Meta


def meta_bytes(event: MIDIMetaEvent) -> bytes:
    ret = bytes()
    if isinstance(event.meta, Tempo):
        # FF 51 03 tttttt, where t is the number of microseconds per beat
        usec_per_quarter = int((60 / event.meta.bpm) * 10**6)
        ret += bytes([0xFF, 0x51, 0x03]) + byted_num(usec_per_quarter, 3)
    if isinstance(event.meta, TimeSignature):
        # FF 58 04 nn dd cc bb
        # n is the numerator, directly
        # d is the denominator as a power of 2 (8 -> 03, 16 -> 04)
        # c is the number of clocks per metronome tick, which we simplify to per denominator
        # b is the number of 32nd notes in a quarter note (8 in our case)
        numerator = event.meta.upper
        denominator = log2(event.meta.lower)
        assert denominator == round(denominator)

        clocks_per_tick = max(1, round(96 * 4 / event.meta.lower))  # we assume 96 clocks per quarter
        ret += bytes([0xFF, 0x58, 0x04, numerator, int(denominator), clocks_per_tick, 0x08])
    if isinstance(event.meta, KeySignature):
        ...  # we could send key signature events
        # but they are restricted to the wheel of fifths and our system allows arbitrary keys

    return ret
