from functools import reduce
from getopt import getopt, GetoptError
from operator import add
from sys import argv, exit

from notetion.midi import event_bytes
from notetion.midi.midi_header import header_bytes, track_bytes
from notetion.parser import Parser
from notetion.scanner import Scanner
from notetion.semantics.semantics_parser import semantics

if __name__ == '__main__':
    inputfile = ''
    outputfile = ''
    helpstring = 'usage: %s -i <input file> -o <output file>' % (argv[0],)

    try:
        opts, args = getopt(argv[1:], "hi:o:", ["in=", "out="])
    except GetoptError:
        print(helpstring)
        exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print(helpstring)
        elif opt in ("-i", "--in"):
            inputfile = arg
        elif opt in ("-o", "--out"):
            outputfile = arg

    if inputfile == '' or outputfile == '':
        print(helpstring)
        exit(2)

    scanner = Scanner(inputfile)
    parser = Parser(scanner)
    midi_notes = semantics(parser)

    header = header_bytes()
    events = reduce(add, map(event_bytes, midi_notes))
    track_header = track_bytes(len(events))

    with open(outputfile, "wb") as f:
        f.write(header)
        f.write(track_header)
        f.write(events)
